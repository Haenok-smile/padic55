/*
 Navicat Premium Data Transfer

 Source Server         : mysql Connection
 Source Server Type    : MySQL
 Source Server Version : 100134
 Source Host           : localhost:3306
 Source Schema         : padic

 Target Server Type    : MySQL
 Target Server Version : 100134
 File Encoding         : 65001

 Date: 23/08/2018 05:31:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_06_21_093455_patient_tb', 1);
INSERT INTO `migrations` VALUES (4, '2018_06_23_072723_physic_tb', 1);
INSERT INTO `migrations` VALUES (5, '2018_06_24_171959_ptpyrel_tb', 1);
INSERT INTO `migrations` VALUES (6, '2018_08_21_110544_modifyusertable', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for patient_tb
-- ----------------------------
DROP TABLE IF EXISTS `patient_tb`;
CREATE TABLE `patient_tb`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mrn` int(11) NOT NULL,
  `first_name` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `midd_name` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `bir_mm` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bir_dd` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bir_yy` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_num1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone_num2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone_num3` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_mm` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_dd` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_yy` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `birthday_msg` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `patient_tb_mrn_unique`(`mrn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of patient_tb
-- ----------------------------
INSERT INTO `patient_tb` VALUES (42, 1938266614, 'testpatient', 'testpatient', NULL, '11', '11', '1990', 'male', 'testpatient@mail.com', NULL, NULL, NULL, 'Ulisa, 123', NULL, 'Vladivostok', 'kray', '08', '22', '2018', '2018-08-22 16:53:33', '2018-08-22 16:53:33', 'Hello, happy birthday!!');
INSERT INTO `patient_tb` VALUES (43, 1737591507, 'John', 'Smith', NULL, '08', '22', '2018', 'male', 'ggg@mail.com', NULL, NULL, NULL, 'Ulisa, 123', '123', 'Vladivostok', 'kray', '08', '22', '2018', '2018-08-22 17:05:48', '2018-08-22 17:20:58', 'hihihihihihihihi');

-- ----------------------------
-- Table structure for physic_tb
-- ----------------------------
DROP TABLE IF EXISTS `physic_tb`;
CREATE TABLE `physic_tb`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pin` int(11) NOT NULL,
  `first_name` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `midd_name` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bir_mm` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bir_dd` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bir_yy` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_num1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_num2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone_num3` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `med_spec` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_mm` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_dd` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_yy` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `physic_tb_pin_unique`(`pin`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ptpyrel_tb
-- ----------------------------
DROP TABLE IF EXISTS `ptpyrel_tb`;
CREATE TABLE `ptpyrel_tb`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ptid` int(11) NOT NULL,
  `pyid` int(11) NOT NULL,
  `att_file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mpid` int(11) NOT NULL,
  `firstname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `spec` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` enum('superAdmin','subAdmin','physician','patient','norole') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'physician',
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photofile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` enum('yes','no') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `phone1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 0, 'first', 'last', 'admin@mail.com', NULL, 'superAdmin', '$2y$10$6uLfK1.IB4EMRjV0FccoAur8B/YARbx.xEILx6Dy40MhB/L.XMNwq', '', 'yes', '5lqclOp5v5ORtpvo9Gphr426dgvpYcilYXY5etddqC8SQVsEEWPQtFxX63lZ', '2018-07-18 01:46:50', '2018-07-18 01:46:50', '', '');
INSERT INTO `users` VALUES (2, 1636648242, 'John', 'Smith', 'volo.ig1012@mail.ru', NULL, 'patient', '$2y$10$krxCNOlbkBi9m0aGUdBv.uEWDpfqda3uWbiNjGsmfPM4TcsUJiXv6', '', 'no', 'AryBaN4LFj1aULkVImWVSjFtJQipcUPldFrdD04mi0fajUKLD35swD8ZAD2r', '2018-08-21 08:52:32', '2018-08-22 15:34:00', '', '');
INSERT INTO `users` VALUES (3, 0, 'maxim', 'georgy', 'doc@mail.com', NULL, 'physician', '$2y$10$5lFghdXtXYIkntrKV8uHseGU03jbfeCUaRQUq.D7BEzSpne0UW7s2', '', 'no', '7Rgy1iP9mGtihbIc7FmEdq7h1lBn9lFaIDpv9yOH5z0R8wuWzcvZSFORgUX1', '2018-08-21 10:29:28', '2018-08-21 10:29:28', '', '');
INSERT INTO `users` VALUES (4, 0, 'John', 'Smith', 'dodo@mail.com', NULL, 'physician', '$2y$10$lag0nAFldmPJDFkUwWsm2ODGusm9A/gdHsjfcpdmeRQy0gIDiTjie', '', 'no', 'IOGMyid7lrY4qx0y0vufPpUZFOPFuw0wEQ4vPqbDCxMIjsJW3Le4TBOUrhIs', '2018-08-21 10:31:22', '2018-08-21 10:31:22', '', '');
INSERT INTO `users` VALUES (5, 0, 'Tesler', 'Molo', 'tesler@mail.com', NULL, 'subAdmin', '$2y$10$oGXRGPEiNsU3dUFAHeatIuEu6QT5mM7mi/uKfJy1WbDywZS7AIM5i', '', 'yes', 'vVdzLmp04SaB9DYBsNs058RVr9FSpEx56pGCgbdnFHVkCStGKbT8sxB6mQqy', '2018-08-21 11:10:49', '2018-08-21 11:10:49', '', '');
INSERT INTO `users` VALUES (6, 1737591507, 'John', 'Smith', 'ggg@mail.com', '', 'patient', '$2y$10$gzDcT5mR4hT85ODQPCBA0uo38fLCffDLonq4d1mX8NpZJPsKnySYe', 'user_photos/P_1737591507.jpg', 'yes', 'dt0TA1ta0TF3lSgaaqbIPzyDGJjqFTbZXEye6bZ8BLiVJK1UBUkQ13ogx6rK', '2018-08-21 11:21:52', '2018-08-22 17:06:12', '', '');
INSERT INTO `users` VALUES (7, 0, 'aaa', 'aaa', 'aaa@mail.com', NULL, 'physician', '$2y$10$PYToZWpT2TL8//GqhjFXdu6qs6QHs4z4VQr2Chsw9rtru7buV.s5G', '', 'no', 'KW8PFitJXXievuNOJlvji5AcaGJs1tQUjGmgc99ruefU6d6fcxdNOVCYV5RC', '2018-08-21 11:29:54', '2018-08-21 11:29:54', '97894567894', '95486257852');
INSERT INTO `users` VALUES (9, 0, 'Boby', 'Filer', 'boby@mail.com', NULL, 'subAdmin', '$2y$10$o15U.DxxVK1rRoSti.skIefgCcmaw1lNznHUhsip3gQsxzPAKK7Tq', '', 'yes', 'E7ysJP1Nt5O8R6gW08a56NyclCXRbLrceKTDTgSTX8T1YOaHdrfxOeLYqhcm', '2018-08-22 13:56:04', '2018-08-22 13:58:27', '94567891234', NULL);

SET FOREIGN_KEY_CHECKS = 1;
